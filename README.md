# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
((UKAZI GIT))
git clone https://licenrih@bitbucket.org/licenrih/stroboskop.git
git status
git add jscolor.js
git commit -a -m "Priprava potrebnih Javascript knjižnic"
git push origin master
```

Naloga 6.2.3:
https://bitbucket.org/licenrih/stroboskop/commits/cb09339e81fbe9f80c0dbdcf78c093af693a271f

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/licenrih/stroboskop/commits/4140a8ae8bb43c08d030c73461bcacc43ddc050e

Naloga 6.3.2:
https://bitbucket.org/licenrih/stroboskop/commits/46766a131ce54fb544133c4a7bb4cfcbe6698cf1

Naloga 6.3.3:
https://bitbucket.org/licenrih/stroboskop/commits/b050421698c4dc51b7629b0daa1967317b1463b7

Naloga 6.3.4:
https://bitbucket.org/licenrih/stroboskop/commits/5bbc3d0d717f53a9ac6983f73e912bb9756e9722

Naloga 6.3.5:

```
((UKAZI GIT))
git checkout master
git status
git merge izgled
git push origin master

```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/licenrih/stroboskop/commits/28b5216ce0633645dbcb08f8bd9d33a3766f1825

Naloga 6.4.2:
https://bitbucket.org/licenrih/stroboskop/commits/78210d22adf7d4856b4d6626a1f1e943904ae99d

Naloga 6.4.3:
https://bitbucket.org/licenrih/stroboskop/commits/9fa62b1de35916cfc88036ccfd0cb78701e446e6

Naloga 6.4.4:
https://bitbucket.org/licenrih/stroboskop/commits/2e23fc5a814b7ee82c7a459d6f9d92a31b3e1f0f